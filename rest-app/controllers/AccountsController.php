<?php

namespace app\controllers;

use yii\rest\ActiveController;

class AccountsController extends ActiveController
{
    public $modelClass = 'app\models\Accounts';

    public function behaviors()
    {
	$behaviors = parent::behaviors();
	unset($behaviors['rateLimiter']);
	return $behaviors;
    }
}

