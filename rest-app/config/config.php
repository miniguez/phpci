<?php
$db = require(__DIR__ . '/db.php');

    return[
        'id'=> 'rest-app',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'app\controllers',
        'aliases' => [
            '@app' => dirname(__DIR__),
        ],
        'components' => [
           'db' => $db,
           'request' => [
	       'parsers' => [
	       'application/json' => 'yii\web\JsonParser',
	       ]
	   ] 
        ]
    ];
