<?php

use PHPUnit\Framework\TestCase;

class RestTest extends TestCase {
    /* función para inicializar curl
     *
     * Parametro: $part_url:String, recibe create, update, view  
     *
     */
    public function init_request($part_url = "")
    {
	$curl = curl_init();
	$url = "http://localhost:8080/index.php?r=accounts".$part_url;
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(            
	    'Content-Type: application/json',
	));

	return $curl;
    }

    public function testPostAccount()
    {        
	$curl = $this->init_request("/create");
	// especifica la petición POST       
	curl_setopt($curl, CURLOPT_POST, 0);

	//datos a insertar 
	$data =  array(           
	    "first_name" => "James",
	    "middle_name" => "",
	    "lastname" => "Smith",
	    "account_balance" => 100.56,
	    "account_number" => 876553567
	);

	// especifica el resultado de post
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	// se adjuntan los datos
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
	//se ejecuta curl
	$result_call = curl_exec($curl);
	//se cierra curl
	curl_close($curl);
	//casteamos el resultado (object) a array
	$result = (array) json_decode($result_call);               
	$this->id_account = $result['id'];
	unset($result['id']);
	//comparamos que el data de entrada sea igual al array resultado
	$this->assertEquals($result, $data);
    } 

    public function testGetAccounts()
    {        
	$curl = $this->init_request();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = json_decode(curl_exec($curl));
	curl_close($curl);

	$this->assertNotEmpty($result);
    }

    // Función para obtener el último registro de la tabla
    public function getLastAccount()
    {
	$curl = $this->init_request();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = json_decode(curl_exec($curl));
	curl_close($curl);

	return end($result);

    }

    public function testGetAccount()
    {
	$account = $this->getLastAccount();

	$curl = $this->init_request('/view&id='.$account->id);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = json_decode(curl_exec($curl));
	curl_close($curl);

	$this->assertEquals($result->first_name, "James");
    }

    // Para la actualización usaremos put
    public function testUpdateAccount()
    {
	$account = $this->getLastAccount();

	$curl = $this->init_request("/update&id=".$account->id);     
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");        
	$data =  array("account_balance" => 1000.5);        
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$result = json_decode(curl_exec($curl));
	curl_close($curl);

	$this->assertEquals($result->account_balance, 1000.5);
    }

    public function testDeteleAccount()
    {
	$account = $this->getLastAccount();

	$curl = $this->init_request("/delete&id=".$account->id);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");  
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	$result = json_decode(curl_exec($curl));
	curl_close($curl);

	$this->assertNull($result);
    }
}

