<?php

namespace app\models;
use Yii;

class Accounts extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
	return 'accounts';
    }

    public function rules()
    {
	return [
	    [['account_balance'], 'number'],
	    [['account_number'], 'integer'],
	    [['first_name', 'middle_name', 'lastname'], 'string', 'max' => 20],
	];
    }

    public function attributeLabels()
    {
	return [
	    'id' => 'ID',
	    'first_name' => 'First Name',
	    'middle_name' => 'Middle Name',
	    'lastname' => 'Lastname',
	    'account_balance' => 'Account Balance',
	    'account_number' => 'Account Number'            
	];
    }
}

