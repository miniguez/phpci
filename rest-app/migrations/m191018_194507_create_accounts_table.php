<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%accounts}}`.
 */
class m191018_194507_create_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%accounts}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(20),
            'middle_name' => $this->string(20),
            'lastname' => $this->string(20),
            'account_balance' => $this->double(2),
            'account_number' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%accounts}}');
    }
}
