<?php
class Math
{
    public function Fibonacci($num) {
	$num1 = 0;
	$num2 = 1;
	$count = 0;

	while ($count < $num-1) {
	    $num3 = $num2 + $num1;
	    $num1 = $num2;
	    $num2 = $num3;
	    $count += 1; 
	}
	return $num1;
    }   
}
?>
