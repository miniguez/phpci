<?php
require __DIR__ . '/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Eris\Generator;

class PropertyTest extends TestCase
{
    use Eris\TestTrait;

    public function testNaturalNumbersMagnitude()
    {
	$this->forAll(
	    Generator\choose(0, 100)
	)
	    ->then(function ($number) {
		$this->assertTrue(
		    $number < 101,
		    "$number is not less than 101 apparently"
		);
	    });
    }
}
