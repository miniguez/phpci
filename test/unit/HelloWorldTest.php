<?php

namespace PHPUnit\Framework;
use PHPUnit\Framework\TestCase;


class HelloWorldTest extends TestCase {
    public function testGreeting() {
	$greeting = "Hello World";
	$requiredGreeting = "Hello World";

	$this->assertEquals($greeting, $requiredGreeting);
    }
}

?>
