<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../math.php';

use PHPUnit\Framework\TestCase;
use Eris\Generator;

class MathTest extends TestCase {
    use Eris\TestTrait;
    
    public function testFibonacci() {
	    $objMath = new Math();
	    $this->assertEquals($objMath->Fibonacci(10), 34);
    }
    
    public function testFibonacciPT() {
	    $objMath = new Math();
	    $this->forAll(
	        Generator\choose(0, 50), $objMath
	    )
	    ->then(function ($number, $objMath) {
		    $this->assertTrue(
		        $objMath->Fibonacci($number) <= 7778742049,
		        $number
		    );
	    });
    }

}
